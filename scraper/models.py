from django.db import models
from unidecode import unidecode


class Author(models.Model):
    name = models.CharField(max_length=50, primary_key=True)
    full_name = models.CharField(max_length=50)

    def __str__(self):
        return self.full_name

    @classmethod
    def create(cls, full_name):
        """ Convenience method to generate name field value from full name """
        author = cls(full_name=full_name)
        # remove Polish characters and whitespaces
        name = unidecode(full_name.lower().replace(" ", ""))
        author.name = name
        return author


class Post(models.Model):
    author = models.ForeignKey(Author, related_name="posts", on_delete=models.CASCADE)
    url = models.URLField(max_length=200, primary_key=True)
    title = models.CharField(max_length=200)
    body = models.TextField()

    def __str__(self):
        return self.title
