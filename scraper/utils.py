# scraper/utils.py
from bs4 import BeautifulSoup
import requests

from .models import Author, Post

# logging setup
import logging

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

BASE_URL = "https://teonite.com"
ENTRYPOINT_URL = "/blog/"


def get_page_soup(url):
    """Return a BeautifulSoup object for an url"""
    full_url = "".join((BASE_URL, url))
    response = requests.get(full_url)
    if response.status_code == 200:
        return BeautifulSoup(response.content, "html.parser")
    else:
        logger.error(f"There was an issue while fetching page: {url}")
        return None


def get_next_page_url(soup):
    """Find a link to next blog page"""
    link = soup.find("a", class_="older-posts")
    if link:
        return link["href"]
    else:
        return None


def get_post_urls(soup):
    """Find links to all posts on a page"""
    posts = soup.find_all("h2", class_="post-title")
    post_urls = [post.a["href"] for post in posts]
    return post_urls


def get_or_create_author(full_name):
    """
    Return existing author object or create a new one
    
    If an author doesn't exist in the database a new record is created.
    """
    author = Author.create(full_name=full_name)
    # insert author into the DB if he/she is not there already
    if not Author.objects.all().filter(full_name=full_name).exists():
        author.save()
    return author


def update_post(url):
    """
    Scrape data from a blog post and insert it into the DB
    
    If a blog post with a given URL already exists in the database its record is updated.
    """
    soup = get_page_soup(url)

    if soup:
        # extract post data
        author_name = soup.find("span", class_="author-content").h4.text
        title = soup.find("h1", class_="post-title").text
        text = soup.find("section", class_="post-content").text

        author = get_or_create_author(author_name)

        Post.objects.update_or_create(
            url=url,
            defaults={"author": author, "url": url, "title": title, "body": text},
        )


def scrape_all_posts():
    """Scrape all posts from the blog"""
    logger.info("Updating all posts")
    next_page_url = ENTRYPOINT_URL
    while next_page_url:
        logger.info(f"Scraping page: {next_page_url}")
        soup = get_page_soup(next_page_url)
        # end scraping if there's a connectivity issue
        if soup is None:
            next_page_url = None
            continue

        post_urls = get_post_urls(soup)
        for url in post_urls:
            update_post(url)

        next_page_url = get_next_page_url(soup)
    logger.info("Finished scraping all available pages")
