# scraper/tests.py
from django.test import TestCase
from bs4 import BeautifulSoup
from pathlib import Path

from .models import Author, Post
from .utils import get_page_soup, get_next_page_url, get_post_urls


class ModelsTest(TestCase):
    def setUp(self):
        self.author = Author.objects.create(
            name="maciejwojcik", full_name="Maciej Wójcik"
        )

        self.post = Post.objects.create(
            author=self.author,
            url="/test/url/",
            title="Test Post",
            body="Some sample text",
        )

    def test_author_string_representation(self):
        author = Author(full_name="Jan Kowalski")
        self.assertEqual(str(author), author.full_name)

    def test_automatic_author_name_generation(self):
        author = Author.create(full_name="Maciej Wójcik")
        self.assertEqual(author.name, "maciejwojcik")

    def test_authors_are_saved_to_database(self):
        author = Author.create(full_name="Radosław Zieliński")
        author.save()
        query = Author.objects.get(name="radoslawzielinski")
        self.assertIsInstance(query, Author)
        self.assertEqual(query.name, "radoslawzielinski")

    def test_post_string_representation(self):
        post = Post(title="A test title")
        self.assertEqual(str(post), post.title)

    def test_post_content(self):
        self.assertEqual(f"{self.post.author}", "Maciej Wójcik")
        self.assertEqual(f"{self.post.url}", "/test/url/")
        self.assertEqual(f"{self.post.title}", "Test Post")
        self.assertEqual(f"{self.post.body}", "Some sample text")

    def test_posts_are_saved_to_db(self):
        Post.objects.create(
            author=self.author,
            url="/another/test/url/",
            title="Another Test Post",
            body="Some sample text",
        )
        query = Post.objects.all()
        self.assertEqual(len(query), 2)


class ScraperTests(TestCase):
    def setUp(self):
        main_page_path = Path.cwd().joinpath(
            "scraper", "test_resources", "main_page.html"
        )
        last_page_path = Path.cwd().joinpath(
            "scraper", "test_resources", "last_page.html"
        )
        blog_post_path = Path.cwd().joinpath(
            "scraper", "test_resources", "blog_post.html"
        )
        with open(main_page_path) as main_file, open(last_page_path) as last_file, open(
            blog_post_path
        ) as blog_post_file:
            self.main_page_soup = BeautifulSoup(main_file, "html.parser")
            self.last_page_soup = BeautifulSoup(last_file, "html.parser")
            self.blog_post_soup = BeautifulSoup(blog_post_file, "html.parser")

    def test_loading_wrong_url_returns_none(self):
        url = "/wrong/url/"
        soup = get_page_soup(url)
        self.assertIsNone(soup)

    def test_next_page_url_is_found(self):
        next_page_url = get_next_page_url(self.main_page_soup)
        self.assertEqual(next_page_url, "https://teonite.com/blog/page/2/")

    def test_next_page_url_is_not_found_on_last_page(self):
        next_page_url = get_next_page_url(self.last_page_soup)
        self.assertIsNone(next_page_url)

    def test_blog_posts_are_found(self):
        posts = get_post_urls(self.main_page_soup)
        self.assertEqual(len(posts), 20)
