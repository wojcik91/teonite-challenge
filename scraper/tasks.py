from __future__ import absolute_import, unicode_literals
from celery import shared_task
from celery.signals import celeryd_after_setup

from .utils import scrape_all_posts


@shared_task()
def hello():
    print("Hello there!")


@shared_task()
def web_scraper():
    scrape_all_posts()


@celeryd_after_setup.connect
def run_scraper_at_startup(**kwargs):
    web_scraper.delay()
