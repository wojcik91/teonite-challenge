# scraper/management/commands/populate_db.py
from django.core.management.base import BaseCommand
from scraper.utils import scrape_all_posts


class Command(BaseCommand):
    help = "A command to manually scrape the blog and update the database"

    def handle(self, *args, **options):
        scrape_all_posts()
