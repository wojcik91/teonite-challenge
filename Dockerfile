# base image
FROM python:3.6
# don't buffer console output
ENV PYTHONUNBUFFERED 1
# remove .pyc files from container
ENV PYTHONDONTWRITEBYTECODE 1
# set working directory
RUN mkdir /code
WORKDIR /code
# install dependencies
ADD requirements.txt /code/
RUN pip install -r requirements.txt
# copy source code
ADD . /code/