# api/utils.py
from collections import Counter

from .stop_words import STOP_WORDS


def get_most_common_words(posts):
    """Return a dictionary of 10 most common words in a group of blog posts"""
    cnt = Counter()
    for post in posts:
        words = tokenize_post(post.body)
        for word in words:
            cnt[word] += 1
        # remove stopwords
        for stop in STOP_WORDS:
            del cnt[stop]
    return {word[0]: word[1] for word in cnt.most_common(10)}


def tokenize_post(post):
    """Split text into a list of words"""
    return post.replace("\n", " ").lower().split(" ")
