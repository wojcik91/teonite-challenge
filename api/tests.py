# api/tests.py
from django.test import TestCase
from django.urls import resolve, reverse
from rest_framework.test import APIClient

from .views import ListStats, ListAuthors, ListAuthorStats
from scraper.models import Author, Post


class ApiTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        author_1 = Author.create("Jan Kowalski")
        author_1.save()
        author_2 = Author.create("Maciej Wójcik")
        author_2.save()
        Post.objects.create(body="test test test test", author=author_1)
        Post.objects.create(
            body="test1 test2 test3 test4 test5 test6 test7 test8 test9 test10",
            author=author_2,
        )


class StatsTests(ApiTests):
    def test_stats_url_exists_and_has_correct_class(self):
        view = resolve(reverse("stats"))
        self.assertEqual(view.func.cls, ListStats)

    def test_stats_are_returned(self):
        response = self.client.get(reverse("stats"))
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.json(), dict)

    def test_returned_stats_are_correct(self):
        response = self.client.get(reverse("stats"))
        self.assertEqual(response.json()["test"], 4)
        self.assertEqual(len(response.json()), 10)


class AuthorsTests(ApiTests):
    def test_authors_url_exists_and_has_correct_class(self):
        view = resolve(reverse("authors"))
        self.assertEqual(view.func.cls, ListAuthors)

    def test_authors_are_returned(self):
        response = self.client.get(reverse("authors"))
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.json(), dict)

    def test_response_contains_all_authors(self):
        response = self.client.get(reverse("authors"))
        self.assertEqual(len(response.json()), 2)


class AuthorStatsTests(ApiTests):
    def test_author_stats_url_exists_and_has_correct_class(self):
        view = resolve(reverse("author-stats", args=[1]))
        self.assertEqual(view.func.cls, ListAuthorStats)

    def test_author_stats_are_returned(self):
        response = self.client.get(reverse("author-stats", args=["jankowalski"]))
        self.assertEqual(response.status_code, 200)
        self.assertIsInstance(response.json(), dict)

    def test_returned_author_stats_are_correct(self):
        response = self.client.get(reverse("author-stats", args=["jankowalski"]))
        self.assertEqual(len(response.json()), 1)
        self.assertEqual(response.json()["test"], 4)
