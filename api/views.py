# api/views.py
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import Http404

from scraper.models import Author, Post
from .utils import get_most_common_words


class ListAuthors(APIView):
    """ View the list of all authors in the database """

    def get(self, request):
        authors = Author.objects.all()
        authors_dict = {author.name: author.full_name for author in authors}
        return Response(authors_dict)


class ListStats(APIView):
    """ View stats for all posts """

    def get(self, request):
        posts = Post.objects.all()
        stats = get_most_common_words(posts)
        return Response(stats)


class ListAuthorStats(APIView):
    """ View stats for a specified author """

    def get(self, request, pk):
        try:
            author = Author.objects.get(name=pk)
        except Author.DoesNotExist:
            raise Http404
        stats = get_most_common_words(author.posts.all())
        return Response(stats)
