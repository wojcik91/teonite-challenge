# api/urls.py
from django.urls import path

from .views import ListAuthors, ListStats, ListAuthorStats

urlpatterns = [
    path("authors/", ListAuthors.as_view(), name="authors"),
    path("stats/", ListStats.as_view(), name="stats"),
    path("stats/<pk>/", ListAuthorStats.as_view(), name="author-stats"),
]
