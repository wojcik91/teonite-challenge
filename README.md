# teonite-challenge

This project is a Django REST API that provides stats about blog posts scraped from [Teonite company blog](https://teonite.com/blog/). It was a test in the recruitment process. The README file is probably a bit unorthodox, because I tried to use it to document my thought process and explain the choices I made.

## Getting started

### Prerequisites

The app is supposed to run in Docker containers, so to run it you need to have `docker` and `docker-compose` installed on your machine. 

### Installation

No installation is required. Just run `docker-compose up` in the terminal and wait for all the containers to start up. If you are running the app for the first time you should wait a bit (about 30 seconds to a minute) before making any requests, because the database needs to be populated.

## Usage

After starting the service up all endpoints should be available at `localhost:8080/`. You can make standard GET requests using `Postman` or `cURL` and the response should be a JSON object.

### `/stats/`

#### Request:

```
curl http://localhost:8080/stats/
```

#### Response:

```
{
    "data":148,
    "new":90,
    "like":89,
    "open":88,
    "also":88,
    "project":87,
    "time":80,
    "use":72,
    "code":71,
    "teonite":69
}
```

### `/authors/`

#### Request:

```
curl http://localhost:8080/authors/
```

#### Response:

```
{
    "paulinamaludy":"Paulina Maludy",
    "jacekchmielewski":"Jacek Chmielewski",
    "lukaszpilatowski":"Łukasz Piłatowski",
    "martakoziel":"Marta Kozieł",
    "robertolejnik":"Robert Olejnik",
    "krystiankur":"Krystian Kur",
    "kamilchudy":"Kamil Chudy",
    "andrzejpiasecki":"Andrzej Piasecki",
    "jaroslawmarciniak":"Jarosław Marciniak",
    "januszgadek":"Janusz Gądek",
    "krzysztofkrzysztofik":"Krzysztof Krzysztofik",
    "michalgryczka":"Michał Gryczka"
}
```

### `/stats/{author}`

#### Request:

```
curl http://localhost:8080/stats/paulinamaludy/
```

#### Response:

```
{
    {"które":33,
    "także":29,
    "teonite":18,
    "stack":18,
    "full":17,
    "roku":15,
    "podczas":15,
    "przez":13,
    "oraz":12,
    "2018":12}
}
```


## Built with
* [Django](https://www.djangoproject.com/)
* [Django REST Framework](https://www.django-rest-framework.org/)
* [Celery](http://www.celeryproject.org/)
* [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/)
* [requests](http://docs.python-requests.org/en/master/)
* [Docker](https://www.docker.com/)

## Timeline

Below is a step by step account of my work during the project.

1. Since I already had some experience with web scraping the first thing I did was write a separate script to get a feel for the blog structure and the data I'd be working with. I also used it to play around with ways to tokenize blog posts and measure word frequency.
2. Went through a couple Django and Django REST Framework tutorials to learn a bit about the tools I was supposed to use.
3. Prepared the dev environment, initialized a new Django project. Deciced to divide functionality between two apps: API and web scraper.
4. Created models for authors and blog posts.
5. To start working on the API itself I needed to populate the database. I could use the admin interface to do it by hand, but since I already had some web scraping code I decided to use it instead to get some real data.
6. Integrated the web scraper into a Django app and created a management command to run it manually. At this point I had all the blog posts in my DB, so I started working on the API.
7. Created endpoint URLs and associated views then hooked it up to a stat counting function. While removing stop words was optional I decided it was simple enough to try and put it together. I decided against using the NLTK Python library, partially because I had some issues getting it to work, but mostly because I had to work with two languages (Polish and English) and NLTK doesn't have Polish stop words by default.
8. Made a separate file with a list of Polish and English stop words. Was surprised to find out it was much quicker (about 3-3.5x lower response time) to delete stop words from the Counter object after tallying everything up than while tokenizing the posts.
9. The big issue now seemed to be getting the web scraper to run asynchronously, but before tackling it I decided to write some tests to make sure everything worked fine.
10. Finally I had to get back to the issue of asynchronously scraping data from the blog. I had heard about Celery before, but I wanted to run just one periodic task, so I thought it was a bit overkill. Ultimately I did not find anything remarkably simpler and Celery seemed to be an industry standard, so I decided to give it a try. After a bit of trial and error I got it to work just fine. Decided to run the scraper at server startup and once every hour after that. It's probably still way to frequent since the blog is not updated that often.
11. I remembered that the app was supposed to be fully operational after running `docker-compose up` so I added a couple commands to `docker-compose.yml` to initialize the database. I'd prefer to do it by hand once and not every time the container starts up, but it was a requirement.
12. All the project requirements were fulfilled, so I tried to prettify everything by running a formatter, adding docstrings and filling out the README.
13. Tested running the app on a fresh install of Ubuntu. Had to change `docker-compose` version, but other than that everything went fine.
14. Didn't expect to attempt configuring a CI pipeline, but after looking into it I thought it looked interesting. Had to google a bit to find out how to build Docker images on a shared GitLab Runner. Got it to build an image and upload it to Docker Hub.

## Challenges

### Custom JSON response

Django abstracts away many aspects of making a web app. There are lots of generic views that generate responses somewhat magically, but calculating and returning stats required writing custom methods. It took some googling, but ultimately it turned out to be easier than expected.

### Asynchronous tasks

In my limited experience with web frameworks I got used to thinking about their request-response cycle, so at first it was a bit hard to figure out how to run something not at all related to an incoming request. The web scraper couldn't run every time a request was made, because a response would take more than 30 seconds. After some research I decided to try out Celery. The setup was a bit complicated, but I got it to work. I still think it's overkill for my simple needs, but I guess at least it's future-proof if I ever decide to flesh out the app.